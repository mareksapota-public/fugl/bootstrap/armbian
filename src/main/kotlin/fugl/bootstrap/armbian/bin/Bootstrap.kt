// Copyright 2020, 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.bin

import com.github.ajalt.clikt.core.CliktCommand
import com.github.ajalt.clikt.output.TermUi
import com.github.ajalt.clikt.parameters.arguments.argument
import com.github.ajalt.clikt.parameters.arguments.convert
import com.github.ajalt.clikt.parameters.groups.OptionGroup
import com.github.ajalt.clikt.parameters.groups.cooccurring
import com.github.ajalt.clikt.parameters.options.convert
import com.github.ajalt.clikt.parameters.options.default
import com.github.ajalt.clikt.parameters.options.flag
import com.github.ajalt.clikt.parameters.options.option
import com.github.ajalt.clikt.parameters.options.required
import com.github.ajalt.clikt.parameters.types.choice
import com.github.ajalt.clikt.parameters.types.int
import fugl.bootstrap.armbian.Device
import fugl.bootstrap.armbian.actions.AddPolicyRcD
import fugl.bootstrap.armbian.actions.AddRootTrim
import fugl.bootstrap.armbian.actions.AddSwapFstab
import fugl.bootstrap.armbian.actions.AddSwapPartition
import fugl.bootstrap.armbian.actions.CopyFiles
import fugl.bootstrap.armbian.actions.CopyImage
import fugl.bootstrap.armbian.actions.CpuGovernor
import fugl.bootstrap.armbian.actions.CreateSwapFilesystem
import fugl.bootstrap.armbian.actions.DisableFirstLogin
import fugl.bootstrap.armbian.actions.DisableSshd
import fugl.bootstrap.armbian.actions.FirstRunCommand
import fugl.bootstrap.armbian.actions.ResizeRoot
import fugl.bootstrap.armbian.actions.SetCpuGovernor
import fugl.bootstrap.armbian.actions.SetHostname
import fugl.bootstrap.armbian.actions.SetRootPassword
import fugl.bootstrap.armbian.exceptions.RootUserRequiredException
import fugl.bootstrap.armbian.helpers.DownloadImage
import fugl.bootstrap.armbian.helpers.OsVersion
import kotlinx.coroutines.runBlocking
import java.io.File
import kotlin.system.exitProcess

class LocalImageOptions : OptionGroup() {
    val image: File by option(
        "--image",
        help = "Image file to put on the SD card",
    )
        .convert { File(it) }
        .required()

    val checksum: File by option(
        "--checksum-file",
        help = "Checksum file for the image",
    )
        .convert { File(it) }
        .required()
}

class RemoteImageOptions : OptionGroup() {
    val workdir: File? by option(
        "--workdir",
        help = "Where to download the image " +
            "(defaults to a temporary directory)",
    )
        .convert { File(it) }
    val board: String by option(
        "--board",
        help = "Board to download an image for",
    ).required()
    val osVersions: List<OsVersion> by option(
        "--os-version",
        help = "Base OS version to use (defaults to newest available OS)",
    )
        .convert { param: String -> OsVersion.fromName(param) }
        .default(OsVersion.ANY)
    val DEFAULT_USE_TORRENT = false
    val useTorrent: Boolean by option(
        "--use-torrent",
        help = "Use torrent to download the image " +
            "(defaults to $DEFAULT_USE_TORRENT)",
    )
        .flag("--no-use-torrent", default = DEFAULT_USE_TORRENT)
}

class Bootstrap : CliktCommand() {
    val DEFAULT_SWAP = 8

    val swapSize: Int by option(
        "--swap-size",
        help = "Swap size in gigabytes (defaults to ${DEFAULT_SWAP}GB)",
    )
        .int()
        .default(DEFAULT_SWAP)

    val hostname: String? by option(
        "--hostname",
        help = "Hostname to use",
    )

    val rootPassword: String? by option(
        "--root-password",
        help = "Root password to use, use @-file parameters to supply the " +
            "password from a file and avoid putting it on command line",
    )

    val DEFAULT_SSHD = false
    val sshd: Boolean by option(
        "--sshd",
        help = "Enable SSHd (defaults to $DEFAULT_SSHD)"
    )
        .flag("--no-sshd", default = DEFAULT_SSHD)

    val DEFAULT_TRIM = false
    val trim: Boolean by option(
        "--trim",
        help = "Enable instant file system TRIM/discard, Debian does this " +
            "periodically by default (defaults to $DEFAULT_TRIM)"
    )
        .flag("--no-trim", default = DEFAULT_TRIM)

    val DEFAULT_CPU_GOVERNOR = CpuGovernor.SCHEDUTIL
    val cpuGovernor: CpuGovernor by option(
        "--cpu-governor",
        help = "CPU frequency governor to use " +
            "(defaults to ${DEFAULT_CPU_GOVERNOR.value})"
    )
        .choice(*CpuGovernor.values().map { it.value }.toTypedArray())
        .convert { param: String -> CpuGovernor.fromValue(param) }
        .default(DEFAULT_CPU_GOVERNOR)

    val firstRunCmd: String? by option(
        "--first-run-command",
        help = "Command to run on first boot, use to bootstrap the system",
    )

    val PAYLOAD_DESTINATION = "/srv/first-run/"
    val firstRunPayload: File? by option(
        "--first-run-payload",
        help = "Directory to copy to $PAYLOAD_DESTINATION",
    )
        .convert { File(it) }

    val localImageOptions: LocalImageOptions? by LocalImageOptions().cooccurring()
    val remoteImageOptions: RemoteImageOptions? by RemoteImageOptions().cooccurring()

    val device: Device by argument(
        help = "SD card device to modify",
    )
        .convert { Device(it) }

    fun assertRoot() {
        if (System.getProperty("user.name") != "root") {
            throw RootUserRequiredException()
        }
    }

    fun confirmDevice() {
        val msg = "This will remove all data on ${device.path}.  " +
            "Do you want to continue?"
        if (TermUi.confirm(msg) != true) {
            exitProcess(2)
        }
    }

    override fun run() = runBlocking {
        assertRoot()
        confirmDevice()
        val image: File
        val checksum: File
        if (localImageOptions !== null) {
            image = localImageOptions!!.image
            checksum = localImageOptions!!.checksum
        } else if (remoteImageOptions !== null) {
            val result = DownloadImage(
                remoteImageOptions!!.workdir,
                remoteImageOptions!!.board,
                remoteImageOptions!!.osVersions,
                remoteImageOptions!!.useTorrent,
            ).run()
            image = result.image
            checksum = result.checksum
        } else {
            throw IllegalArgumentException(
                "No local or remote image options given",
            )
        }
        listOf(
            CopyImage(device, image, checksum),
            AddSwapPartition(device, swapSize),
            ResizeRoot(device, swapSize),
        ).forEach { it.run() }
        val rootDevice = device.partition(1)
        val swapDevice = device.partition(2)
        listOf(
            CreateSwapFilesystem(swapDevice),
            AddSwapFstab(rootDevice, swapDevice, trim),
            AddRootTrim(rootDevice, trim),
            SetHostname(rootDevice, hostname),
            AddPolicyRcD(rootDevice),
            DisableSshd(rootDevice, !sshd),
            SetCpuGovernor(rootDevice, cpuGovernor)
        ).forEach { it.run() }
        if (rootPassword != null) {
            listOf(
                DisableFirstLogin(rootDevice),
                SetRootPassword(rootDevice, rootPassword!!),
            ).forEach { it.run() }
        }
        if (firstRunCmd !== null) {
            FirstRunCommand(rootDevice, firstRunCmd!!).run()
        }
        if (firstRunPayload !== null) {
            CopyFiles(rootDevice, firstRunPayload!!, PAYLOAD_DESTINATION).run()
        }
    }
}

fun main(args: Array<String>) = Bootstrap().main(args)
