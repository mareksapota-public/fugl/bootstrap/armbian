// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import org.sapota.marek.shell_command.ShellCommand
import java.io.File
import kotlin.io.path.createTempDirectory

@OptIn(kotlin.io.path.ExperimentalPathApi::class)
abstract class MountAction(device: Device) : BaseAction(device) {
    private var mountPointFile: File? = null

    protected val mountPoint: File
        get() = mountPointFile!!

    private suspend fun withMount(block: suspend () -> Unit) {
        mountPointFile = File(createTempDirectory().toString())
        mountPoint.deleteOnExit()
        val mountCommand = ShellCommand("mount", device.path, mountPoint.path)
        val umountCommand = ShellCommand("umount", "-l", device.path)
        println("Mounting ${device.path} at ${mountPoint.path}")
        executor.run(mountCommand)
        try {
            block()
        } finally {
            println("Unmounting ${device.path}")
            executor.run(umountCommand)
        }
    }

    override suspend fun runEachAction() {
        this.withMount {
            super.runEachAction()
        }
    }
}
