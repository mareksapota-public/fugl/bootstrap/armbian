// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import fugl.bootstrap.armbian.exceptions.ChecksumFailedException
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellExecutor
import org.sapota.marek.shell_command.ShellSpecialCharacter
import java.io.File

class CopyImage(
    device: Device,
    protected val imageFile: File,
    protected val checksumFile: File,
) : BaseAction(device) {
    override fun message(): String? {
        return "Verifying and copying image to SD card"
    }

    suspend fun checkImage() {
        val checksums = checksumFile.readLines().map { line ->
            val splitLine = line.split(" ", limit = 2)
            val sha = splitLine.first()
            val fileName = splitLine.last()
            listOf(
                Pair(fileName, sha),
                Pair(fileName.replace(Regex("^\\*"), ""), sha),
            )
        }.flatten().toMap()
        if (!checksums.containsKey(imageFile.name)) {
            throw ChecksumFailedException("Checksum for image not found")
        }
        val cmd = ShellCommand(
            "echo",
            "${checksums[imageFile.name]} ${imageFile.path}",
        )
        cmd.add(ShellSpecialCharacter.PIPE)
        cmd.addAll(
            "sha256sum",
            "-c",
        )
        ShellExecutor(autoShellWrap = true).run(cmd)
    }

    override suspend fun runActions(): List<ShellCommand> {
        checkImage()
        val cmd = ShellCommand(
            "xzcat",
            "${imageFile.path}",
        )
        cmd.add(ShellSpecialCharacter.PIPE)
        cmd.addAll(
            "dd",
            "of=${device.path}",
            "bs=4MB",
        )
        return listOf(cmd).plus(this.syncPartitionTable())
    }
}
