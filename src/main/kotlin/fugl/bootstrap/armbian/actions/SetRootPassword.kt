// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellExecutor
import org.sapota.marek.shell_command.ShellSpecialCharacter
import java.io.File

class SetRootPassword(device: Device, val password: String) : MountAction(device) {
    override fun message(): String? {
        return "Setting root password"
    }

    override suspend fun runActions(): List<ShellCommand> {
        val passwdCmd = ShellCommand("echo", password)
        passwdCmd.add(ShellSpecialCharacter.PIPE)
        passwdCmd.addAll("openssl", "passwd", "-6", "-stdin")
        val passwdExecutor = ShellExecutor(
            autoShellWrap = true,
            captureOutput = true,
        )
        passwdExecutor.run(passwdCmd)
        val passwordHash = passwdExecutor.getStdout()

        val shadow = File("${mountPoint.path}/etc/shadow")
        val awkCmd = ShellCommand("cat", shadow.path)
        awkCmd.add(ShellSpecialCharacter.PIPE)
        awkCmd.addAll(
            "awk",
            "-F",
            ":",
            "BEGIN { OFS=\":\" } { (\$1 == \"root\") ? " +
                "\$2=\"$passwordHash\" : \$2=\$2; print }",
        )
        awkCmd.add(ShellSpecialCharacter.PIPE)
        awkCmd.addAll("tee", "${shadow.path}.new")
        val chmodCmd = ShellCommand(
            "chmod",
            "--reference",
            shadow.path,
            "${shadow.path}.new",
        )
        val moveCmd = ShellCommand("mv", "${shadow.path}.new", shadow.path)
        return listOf(awkCmd, chmodCmd, moveCmd)
    }
}
