// Copyright 2020, 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellSpecialCharacter
import java.io.File

enum class CpuGovernor() {
    CONSERVATIVE,
    ONDEMAND,
    USERSPACE,
    POWERSAVE,
    PERFORMANCE,
    SCHEDUTIL;

    val value: String
        get() = this.name.toLowerCase()

    companion object {
        fun fromValue(value: String): CpuGovernor {
            return enumValueOf(value.toUpperCase())
        }
    }
}

class SetCpuGovernor(device: Device, val governor: CpuGovernor) : MountAction(device) {
    override fun message(): String? {
        return "Setting CPU frequency governor"
    }

    override suspend fun runActions(): List<ShellCommand> {
        val configFile = File("${mountPoint.path}/etc/default/cpufrequtils")
        val awkCmd = ShellCommand("cat", configFile.path)
        awkCmd.add(ShellSpecialCharacter.PIPE)
        awkCmd.addAll(
            "awk",
            "-F",
            "=",
            "BEGIN { OFS=\"=\" } { (\$1 == \"GOVERNOR\") ? " +
                "\$2=\"${governor.value}\" : \$2=\$2; print }",
        )
        awkCmd.add(ShellSpecialCharacter.PIPE)
        awkCmd.addAll("tee", "${configFile.path}.new")
        val chmodCmd = ShellCommand(
            "chmod",
            "--reference",
            configFile.path,
            "${configFile.path}.new",
        )
        val moveCmd = ShellCommand(
            "mv",
            "${configFile.path}.new",
            configFile.path,
        )
        return listOf(awkCmd, chmodCmd, moveCmd)
    }
}
