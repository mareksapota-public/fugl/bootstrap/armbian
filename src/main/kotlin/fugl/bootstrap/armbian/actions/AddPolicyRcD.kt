// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellSpecialCharacter

class AddPolicyRcD(device: Device) : MountAction(device) {
    override fun message(): String? {
        return "Disable service restart (or start) on dpkg-reconfigure"
    }

    override suspend fun runActions(): List<ShellCommand> {
        val rcScript = """#!/bin/bash
exit 101
""".replace("\n", "\\n")
        val rcCommand = ShellCommand(
            "echo",
            "-e",
            "-n",
            rcScript,
        )
            .add(ShellSpecialCharacter.REDIRECT_OUTPUT)
            .add("$mountPoint/usr/sbin/policy-rc.d")
            .shellWrap()
        return listOf(
            rcCommand,
            ShellCommand(
                "chmod",
                "+x",
                "$mountPoint/usr/sbin/policy-rc.d",
            ),
        )
    }
}
