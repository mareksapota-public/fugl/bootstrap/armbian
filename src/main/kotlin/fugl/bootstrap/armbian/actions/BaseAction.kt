// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellExecutor

abstract class BaseAction(protected val device: Device) {
    protected fun syncPartitionTable(): List<ShellCommand> {
        return listOf(
            ShellCommand("sync"),
            ShellCommand("partprobe"),
        )
    }

    open suspend fun runActions(): List<ShellCommand> {
        return listOf()
    }

    suspend fun run() {
        val msg = this.message()
        if (msg !== null) {
            println(msg)
        }
        runEachAction()
    }

    open suspend fun runEachAction() {
        runActions().forEach { command ->
            println("Running: $command")
            executor.run(command)
        }
    }

    open fun message(): String? {
        return null
    }

    companion object {
        val executor = ShellExecutor(autoShellWrap = true)
    }
}
