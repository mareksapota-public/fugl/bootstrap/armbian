// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import org.sapota.marek.shell_command.ShellCommand

class DisableFirstLogin(device: Device) : MountAction(device) {
    override fun message(): String? {
        return "Disable Armbian first login"
    }

    override suspend fun runActions(): List<ShellCommand> {
        val rmFlagFile = ShellCommand(
            "rm",
            "${mountPoint.path}/root/.not_logged_in_yet",
        )
        // FUGL resizes the filesystem, this service running breaks the swap
        // partition until `mkswap` is run again.
        val disableFilesystemResize = ShellCommand(
            "rm",
            "-f",
            "${mountPoint.path}/etc/systemd/system/" +
                "basic.target.wants/armbian-resize-filesystem.service"
        )
        // See https://github.com/armbian/build/blob/397d037089db43a14851d7434c5cdaa46f166068/packages/bsp/common/usr/lib/armbian/armbian-firstlogin#L188
        val disableAutologinCmd = ShellCommand(
            "rm",
            "-f",
            "${mountPoint.path}/etc/systemd/system/" +
                "getty@.service.d/override.conf",
        )
        val disableAutologinSerialCmd = ShellCommand(
            "rm",
            "-f",
            "${mountPoint.path}/etc/systemd/system/" +
                "serial-getty@.service.d/override.conf",
        )
        return listOf(
            rmFlagFile,
            disableFilesystemResize,
            disableAutologinCmd,
            disableAutologinSerialCmd,
        )
    }
}
