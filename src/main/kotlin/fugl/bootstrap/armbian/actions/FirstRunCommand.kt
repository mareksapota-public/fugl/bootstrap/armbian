// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellSpecialCharacter

class FirstRunCommand(device: Device, val command: String) : MountAction(device) {
    override fun message(): String? {
        return "Enable first run command"
    }

    override suspend fun runActions(): List<ShellCommand> {
        val serviceName = "fugl-bootstrap-first-run"
        val startCommand = ShellCommand(command)
        val firstRunSh = """#!/bin/bash
set -euo pipefail
$startCommand
systemctl disable $serviceName
rm /srv/$serviceName.sh
""".replace("\n", "\\n")
        val writeFirstRunSh = ShellCommand(
            "echo",
            "-e",
            "-n",
            firstRunSh,
        )
            .add(ShellSpecialCharacter.REDIRECT_OUTPUT)
            .add("$mountPoint/srv/$serviceName.sh")
        val service = """[Unit]
Description=FUGL-bootstrap first run service

[Service]
ExecStart=/srv/$serviceName.sh

[Install]
WantedBy=multi-user.target
""".replace("\n", "\\n")
        val serviceFile = "/usr/lib/systemd/system/$serviceName.service"
        val writeServiceCmd = ShellCommand(
            "echo",
            "-e",
            "-n",
            service,
        )
            .add(ShellSpecialCharacter.REDIRECT_OUTPUT)
            .add("${mountPoint}$serviceFile")
        return listOf(
            writeFirstRunSh,
            ShellCommand("chmod", "+x", "$mountPoint/srv/$serviceName.sh"),
            writeServiceCmd.shellWrap(),
            ShellCommand(
                "ln",
                "-s",
                serviceFile,
                "$mountPoint/etc/systemd/system/" +
                    "multi-user.target.wants/$serviceName.service",
            ),
        )
    }
}
