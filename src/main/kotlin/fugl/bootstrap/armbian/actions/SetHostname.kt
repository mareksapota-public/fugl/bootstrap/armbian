// Copyright 2020 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.actions

import fugl.bootstrap.armbian.Device
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellSpecialCharacter
import java.io.File

class SetHostname(device: Device, val hostname: String?) : MountAction(device) {
    override fun message(): String? {
        return "Setting hostname"
    }

    override suspend fun runActions(): List<ShellCommand> {
        if (hostname == null) {
            println("No hostname to set")
            return listOf()
        }
        val etcHostnameCmd = ShellCommand("echo", hostname)
        etcHostnameCmd.add(ShellSpecialCharacter.PIPE)
        etcHostnameCmd.addAll("tee", "$mountPoint/etc/hostname")

        val sedV4Cmd = ShellCommand(
            "sed",
            "-e",
            "s/^127\\.0\\.0\\.1[\\t ].*/127.0.0.1 localhost $hostname/g",
        )
        val sedV6Cmd = ShellCommand(
            "sed",
            "-e",
            "s/^::1[\\t ].*/::1 localhost ipv6-localhost ipv6-loopback $hostname/g",
        )

        val hosts = File("${mountPoint.path}/etc/hosts")
        val hostsCmd = ShellCommand("cat", hosts.path)
        hostsCmd.add(ShellSpecialCharacter.PIPE)
        hostsCmd.addAll(sedV4Cmd)
        hostsCmd.add(ShellSpecialCharacter.PIPE)
        hostsCmd.addAll(sedV6Cmd)
        hostsCmd.add(ShellSpecialCharacter.PIPE)
        hostsCmd.addAll("tee", "${hosts.path}.new")
        val moveCmd = ShellCommand("mv", "${hosts.path}.new", hosts.path)

        return listOf(etcHostnameCmd, hostsCmd, moveCmd)
    }
}
