// Copyright 2020, 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.helpers

enum class OsVersion(val slug: String) {
    DEBIAN_11_BULLSEYE_MINIMAL("Bullseye_current_minimal"),
    DEBIAN_11_BULLSEYE("Bullseye_current"),
    DEBIAN_11_BULLSEYE_DESKTOP("Bullseye_current_desktop"),
    UBUNTU_20_04_FOCAL_MINIMAL("Focal_current_minimal"),
    UBUNTU_20_04_FOCAL("Focal_current"),
    UBUNTU_20_04_FOCAL_DESKTOP("Focal_current_desktop"),
    DEBIAN_10_BUSTER_MINIMAL("Buster_current_minimal"),
    DEBIAN_10_BUSTER("Buster_current"),
    DEBIAN_10_BUSTER_DESKTOP("Buster_current_desktop"),
    UBUNTU_18_04_BIONIC_MINIMAL("Bionic_current_minimal"),
    UBUNTU_18_04_BIONIC("Bionic_current"),
    UBUNTU_18_04_BIONIC_DESKTOP("Bionic_current_desktop");

    companion object {
        val DEBIAN_11 = listOf(
            DEBIAN_11_BULLSEYE_MINIMAL,
            DEBIAN_11_BULLSEYE,
            DEBIAN_11_BULLSEYE_DESKTOP,
        )
        val DEBIAN_10 = listOf(
            DEBIAN_10_BUSTER_MINIMAL,
            DEBIAN_10_BUSTER,
            DEBIAN_10_BUSTER_DESKTOP,
        )
        val DEBIAN = DEBIAN_11.plus(DEBIAN_10)

        val UBUNTU_20_04 = listOf(
            UBUNTU_20_04_FOCAL_MINIMAL,
            UBUNTU_20_04_FOCAL,
            UBUNTU_20_04_FOCAL_DESKTOP,
        )
        val UBUNTU_18_04 = listOf(
            UBUNTU_18_04_BIONIC_MINIMAL,
            UBUNTU_18_04_BIONIC,
            UBUNTU_18_04_BIONIC_DESKTOP,
        )
        val UBUNTU = UBUNTU_20_04.plus(UBUNTU_18_04)

        val MINIMAL = listOf(
            DEBIAN_11_BULLSEYE_MINIMAL,
            UBUNTU_20_04_FOCAL_MINIMAL,
            DEBIAN_10_BUSTER_MINIMAL,
            UBUNTU_18_04_BIONIC_MINIMAL,
        )
        val DESKTOP = listOf(
            DEBIAN_11_BULLSEYE_DESKTOP,
            UBUNTU_20_04_FOCAL_DESKTOP,
            DEBIAN_10_BUSTER_DESKTOP,
            UBUNTU_18_04_BIONIC_DESKTOP,
        )
        val STANDARD = listOf(
            DEBIAN_11_BULLSEYE,
            UBUNTU_20_04_FOCAL,
            DEBIAN_10_BUSTER,
            UBUNTU_18_04_BIONIC,
        )

        val ANY = DEBIAN_11.plus(UBUNTU_20_04).plus(DEBIAN_10)
            .plus(UBUNTU_18_04)

        fun fromName(name: String): List<OsVersion> {
            try {
                return listOf(OsVersion.valueOf(name))
            } catch (_: IllegalArgumentException) {}
            return when (name.toLowerCase()) {
                "debian" -> DEBIAN
                "debian11" -> DEBIAN_11
                "debian_11" -> DEBIAN_11
                "bullseye" -> DEBIAN_11
                "debian10" -> DEBIAN_10
                "debian_10" -> DEBIAN_10
                "buster" -> DEBIAN_10
                "ubuntu" -> UBUNTU
                "ubuntu20.04" -> UBUNTU_20_04
                "ubuntu_20.04" -> UBUNTU_20_04
                "ubuntu_focal" -> UBUNTU_20_04
                "focal" -> UBUNTU_20_04
                "ubuntu18.04" -> UBUNTU_18_04
                "ubuntu_18.04" -> UBUNTU_18_04
                "ubuntu_bionic" -> UBUNTU_18_04
                "bionic" -> UBUNTU_18_04
                "desktop" -> DESKTOP
                "standard" -> STANDARD
                "minimal" -> MINIMAL
                "autodetect" -> ANY
                else -> {
                    throw IllegalArgumentException("Unknown OS version $name")
                }
            }
        }
    }
}
