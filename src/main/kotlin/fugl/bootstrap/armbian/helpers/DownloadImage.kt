// Copyright 2020, 2021 Marek Sapota

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

package fugl.bootstrap.armbian.helpers

import fugl.bootstrap.armbian.exceptions.ImageDownload404Exception
import org.sapota.marek.shell_command.ShellCommand
import org.sapota.marek.shell_command.ShellExecutor
import org.sapota.marek.shell_command.ShellSpecialCharacter
import java.io.File
import kotlin.io.path.createTempDirectory

data class ImageLocation(
    val image: File,
    val checksum: File,
)

data class RawUrl(val value: String)

data class ResolvedUrl(val value: String)

data class DownloadUrls(val image: ResolvedUrl, val checksum: ResolvedUrl)

@OptIn(kotlin.io.path.ExperimentalPathApi::class)
class DownloadImage(
    val selectedWorkdir: File?,
    val board: String,
    val osVersions: Iterable<OsVersion>,
    val useTorrent: Boolean,
) {
    private suspend fun resolveUrl(url: RawUrl): ResolvedUrl {
        val locationCmd = ShellCommand("curl", "-vv", url.value)
        locationCmd.add(ShellSpecialCharacter.REDIRECT_STDERR_TO_STDOUT)
        locationCmd.add(ShellSpecialCharacter.PIPE)
        locationCmd.addAll("grep", "location:")
        locationCmd.add(ShellSpecialCharacter.PIPE)
        locationCmd.addAll("awk", "{ print \$3; }")
        val locationExecutor = ShellExecutor(
            autoShellWrap = true,
            captureOutput = true,
        )
        locationExecutor.run(locationCmd)
        return ResolvedUrl(locationExecutor.getStdout())
    }

    private suspend fun isUrlValid(url: ResolvedUrl): Boolean {
        val checkCmd = ShellCommand("curl", "-I", url.value)
        val checkExecutor = ShellExecutor(captureOutput = true)
        checkExecutor.run(checkCmd)
        val stdout = checkExecutor.getStdout().toLowerCase()
        return !stdout.contains("404") &&
            stdout.contains("content-type: application/octet-stream")
    }

    private fun archiveName(url: ResolvedUrl): String {
        return url.value.split("/").last()
    }

    private suspend fun download(workdir: File, url: ResolvedUrl): String {
        val fileName = archiveName(url)
        if (!downloadExists(workdir, fileName)) {
            val downloadCmd: ShellCommand = ShellCommand(
                "wget",
                url.value,
            )
            val downloadExecutor = ShellExecutor(cwd = workdir)
            downloadExecutor.run(downloadCmd)
        }
        return fileName
    }

    private fun downloadExists(workdir: File, fileName: String): Boolean {
        val downloadPath = File(
            workdir.path +
                File.separator +
                fileName
        )
        if (downloadPath.exists()) {
            println("${downloadPath.path} already exists, skipping download")
            return true
        }
        return false
    }

    private suspend fun downloadTorrent(
        workdir: File,
        torrentFile: String,
    ): String {
        val fileName = torrentFile.split(".").let { parts ->
            parts.take(parts.size - 1)
        }.joinToString(".")
        if (!downloadExists(workdir, fileName)) {
            val torrentCmd = ShellCommand(
                "lftp",
                "-c",
                "torrent $torrentFile",
            )
            val torrentExecutor = ShellExecutor(cwd = workdir)
            torrentExecutor.run(torrentCmd)
        }
        return fileName
    }

    suspend fun findDownloadUrls(): DownloadUrls {
        val baseUrl = "https://redirect.armbian.com/region/NA/$board/"
        osVersions.forEach { osVersion ->
            val slugUrl = baseUrl + osVersion.slug
            val url = RawUrl(slugUrl + (if (useTorrent) ".torrent" else ""))
            val downloadUrls = DownloadUrls(
                resolveUrl(url),
                resolveUrl(RawUrl(url.value + ".sha")),
            )
            if (isUrlValid(downloadUrls.image)) {
                return downloadUrls
            } else {
                println("Skipping unavailable image: ${url.value}")
            }
        }
        throw ImageDownload404Exception()
    }

    suspend fun run(): ImageLocation {
        val workdir: File
        if (selectedWorkdir == null) {
            workdir = File(createTempDirectory().toString())
            workdir.deleteOnExit()
        } else {
            workdir = selectedWorkdir
        }

        val downloadUrls = findDownloadUrls()
        var downloadArchive = download(workdir, downloadUrls.image)

        if (useTorrent) {
            downloadArchive = downloadTorrent(workdir, downloadArchive)
        }

        val checksumArchive: String = download(workdir, downloadUrls.checksum)

        return ImageLocation(
            File("$workdir/$downloadArchive"),
            File("$workdir/$checksumArchive"),
        )
    }
}
